import users.UserCollection;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ServletLogin extends HttpServlet {
    private UserCollection users;

    public ServletLogin(UserCollection users) {
        this.users = users;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Files.copy(Paths.get("formLogin.html"), resp.getOutputStream());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);

        String login = pfr.getString("login");

        resp.addCookie(new Cookie("calc_cookie", login));
        resp.sendRedirect("/calc");
    }
}
