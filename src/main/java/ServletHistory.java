import users.UsersActions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class ServletHistory extends HttpServlet {
    private UsersActions usersActions;

    public ServletHistory(UsersActions usersActions) {
        this.usersActions = usersActions;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter writer = resp.getWriter();

        Arrays.stream(req.getCookies())
                .filter(c -> c.getName().equalsIgnoreCase("calc_cookie"))
                .findFirst()
                .ifPresent(c -> {
                    String link = "<a href=\"/calc\">New calculation</a>";
                    writer.printf("<html><p>Your history:</p><p>%s</p> <p>%s</p></html>", usersActions.getUserActions(c.getValue()), link);
                });
    }
}
