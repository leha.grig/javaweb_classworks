
import commands.Command;
import commands.Commands;
import users.User;
import users.UserCollection;
import users.UsersActions;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class ServletCalc extends HttpServlet {
    private List<Command> commands = Commands.allCommands();
    private UserCollection <User, HashMap<String, User>> users;
    private User user;
    private UsersActions usersActions;


    public ServletCalc(UserCollection users, UsersActions usersActions) {
        this.users = users;
        this.usersActions = usersActions;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie[] cookies = req.getCookies();
            Arrays.stream(cookies)
                    .filter(c -> c.getName().equalsIgnoreCase("calc_cookie"))
                    .findFirst()
                    .ifPresent(c -> {
                        user = users.getUser(c.getValue());
                        try {
                            Files.copy(Paths.get("form_calc.html"), resp.getOutputStream());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }

        /*if(session.getAttribute("isLoggedIn")!=null && session.getAttribute("isLoggedIn").equals(true)) {
            Files.copy(Paths.get("form_calc.html"), resp.getOutputStream());
        }*/


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ParameterFromRequest pfr = new ParameterFromRequest(req);
        PrintWriter writer = resp.getWriter();


        int xNum = pfr.getInt("x");
        int yNum = pfr.getInt("y");
        String op = pfr.getString("op");

        Command cmd;
        cmd = commands.stream()
                .filter(c -> c.getName().equalsIgnoreCase(op))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("This case should never happen"));

        int result = cmd.doCommand(xNum, yNum);
        String operation = cmd.getSymbol();
        String resultStr = String.format("%d %s %d = %d", xNum, operation, yNum, result);
        usersActions.addUserActions(user.getLogin(), resultStr);
        writer.printf("<html><p>%s </p> <p> <a href=\"/calc\">New calculation</a></p></html>", resultStr);

    }
}

