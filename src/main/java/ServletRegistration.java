import users.User;
import users.UserCollection;
import users.UsersActions;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServletRegistration extends HttpServlet {
    private UserCollection<User, HashMap<String, User>> users;
    private UsersActions usersActions;

    public ServletRegistration(UserCollection users, UsersActions usersActions) {
        this.users = users;
        this.usersActions = usersActions;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Files.copy(Paths.get("formAuthorization.html"), resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);
        PrintWriter writer = resp.getWriter();
        String name = pfr.getString("name").trim();
        String surname = pfr.getString("surname").trim();
        String login = pfr.getString("login").trim();
        String password = pfr.getString("password").trim();

        User user = new User(name, surname, login, password);
        users.addUser(user);
        List<String> actions = new ArrayList<>();
        usersActions.addUser(user.getLogin(), actions);

        resp.sendRedirect("/login");
    }
}
