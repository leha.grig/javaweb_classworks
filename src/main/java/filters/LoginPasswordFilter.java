package filters;

import org.eclipse.jetty.http.HttpMethod;
import users.UserCollection;
import utils.ParameterFromRequest;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LoginPasswordFilter implements Filter {
    private UserCollection users;

    public LoginPasswordFilter(UserCollection users) {
        this.users = users;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        if (request instanceof HttpServletRequest) {
            req = (HttpServletRequest) request;
        } else {
            throw new IllegalArgumentException("ServletRequest should be instance of HttpServletRequest");
        }

        ParameterFromRequest pfr = new ParameterFromRequest(req);
        String login = pfr.getString("login");
        String password = pfr.getString("password");

        if (HttpMethod.POST.name().equalsIgnoreCase(req.getMethod())) {

            if (!users.check(login, password)) {
                try {
                    throw new IllegalArgumentException("The user login or password is incorrect");
                } catch (IllegalArgumentException e) {
                    response.getWriter().printf("<html> <a href=\"/login\">%s</a></html>", e.getMessage());
                }
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }


    @Override
    public void destroy() {

    }
}
