package filters;

import org.eclipse.jetty.http.HttpMethod;
import users.User;
import users.UserCollection;
import utils.ParameterFromRequest;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class ExistingLoginFilter implements Filter {
    private UserCollection<User, HashMap<String, User>> users;

    public ExistingLoginFilter(UserCollection users) {
        this.users = users;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        if (request instanceof HttpServletRequest) {
            req = (HttpServletRequest) request;
        } else {
            throw new IllegalArgumentException("ServletRequest should be instance of HttpServletRequest");
        }
        if (HttpMethod.POST.name().equalsIgnoreCase(req.getMethod())) {
            ParameterFromRequest pfr = new ParameterFromRequest(req);
            PrintWriter writer = response.getWriter();
            String login = pfr.getString("login").trim();
            if (users.getUsersList().size() == 0) {
                chain.doFilter(request, response);
            }
            users.getUsersList().forEach(user -> {
                if (user.getLogin().equalsIgnoreCase(login)) {
                    try {
                        throw new IllegalArgumentException("User with this login is already exist");
                    } catch (IllegalArgumentException e) {
                        writer.printf("<html> <a href=\"/registration\"> %s </a></html>", e.getMessage());
                    }
                } else {
                    try {
                        chain.doFilter(request, response);
                    } catch (IOException | ServletException e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
