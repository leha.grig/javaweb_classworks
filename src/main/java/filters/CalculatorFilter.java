package filters;

import commands.MathActions;
import org.eclipse.jetty.http.HttpMethod;
import utils.ParameterFromRequest;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;

public class CalculatorFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        if (request instanceof HttpServletRequest) {
            req = (HttpServletRequest)request;
        } else {
            throw new IllegalArgumentException("ServletRequest should be instance of HttpServletRequest");
        }
        if (HttpMethod.POST.name().equalsIgnoreCase(req.getMethod())) {
            PrintWriter writer = response.getWriter();
            try {
                ParameterFromRequest pfr = new ParameterFromRequest(req);
                int xNum = pfr.getInt("x");
                int yNum = pfr.getInt("y");
                String op = pfr.getString("op");

                if (!MathActions.getStringValues().contains(op)){
                    throw new IllegalStateException("Operation is not correct");
                }
                chain.doFilter(request, response);
            } catch (IllegalStateException e) {
                writer.println(e.getMessage());
            } catch (NumberFormatException e) {
                writer.println("Integer conversion error");
            } catch (RuntimeException e) {
                writer.println(e.getMessage());
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
}
