package filters;

import org.eclipse.jetty.http.HttpMethod;
import users.User;
import users.UserCollection;
import utils.ParameterFromRequest;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class LoginFormatFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        if (request instanceof HttpServletRequest) {
            req = (HttpServletRequest) request;
        } else {
            throw new IllegalArgumentException("ServletRequest should be instance of HttpServletRequest");
        }
        if (HttpMethod.POST.name().equalsIgnoreCase(req.getMethod())) {
            ParameterFromRequest pfr = new ParameterFromRequest(req);
            PrintWriter writer = response.getWriter();
            String login = pfr.getString("login").trim();
            if (!login.matches("[\\w_.-]{1,20}")) {
                try {
                    throw new IllegalArgumentException("Incorrect login format! Login can contain from 1 to 20 letters, digits or symbols '.', '-', '_' only");
                } catch (IllegalArgumentException e){
                    writer.printf("<html> <a href=\"/registration\"> %s </a></html>", e.getMessage());
                }
            } else {
                chain.doFilter(request, response);
            }

        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}

