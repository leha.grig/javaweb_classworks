package filters;

import users.User;
import users.UserCollection;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class CookieMatchFilter implements Filter {
    private UserCollection <String, HashMap<String, User>>users;

    public CookieMatchFilter (UserCollection users) {
        this.users = users;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req;
        HttpServletResponse resp;
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            req = (HttpServletRequest) request;
            resp = (HttpServletResponse) response;
        } else {
            throw new IllegalArgumentException("ServletRequest and ServletResponse should be instance of HttpServletRequest and HttpServletResponse");
        }

        Cookie[] cookies = req.getCookies();

            Arrays.stream(cookies)
                    .filter(c -> c.getName().equalsIgnoreCase("calc_cookie"))
                    .findFirst()
                    .ifPresent(c -> {  if (users.getUsers().keySet().contains(c.getValue())){
                        try {
                            chain.doFilter(request, response);
                        } catch (IOException | ServletException e) {
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            resp.sendRedirect("/login");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }});
    }

    @Override
    public void destroy() {

    }
}
