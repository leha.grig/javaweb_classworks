import filters.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import users.UserCollection;
import users.UserCollectionHashMap;
import users.UsersActions;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class App {

    // TODO add filters for login and others
    // TODO скачать и установить datagrip?

    public static void main(String[] args) throws Exception {
        UserCollection users = new UserCollectionHashMap();
        UsersActions usersActions = new UsersActions();
        ServletExample servlet1 = new ServletExample();
        ServletCalc servletCalc = new ServletCalc(users, usersActions);
        ServletRegistration servletRegistration = new ServletRegistration(users, usersActions);
        ServletLogin servletLogin = new ServletLogin(users);
        ServletLogout servletLogout = new ServletLogout();
        ServletHistory servletHistory = new ServletHistory(usersActions);

        System.out.println("Hello");

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(servlet1), "/admin");
        handler.addServlet(new ServletHolder(servletCalc), "/calc");
        handler.addServlet(new ServletHolder(servletRegistration), "/registration");
        handler.addServlet(new ServletHolder(servletLogin), "/login");
        handler.addServlet(new ServletHolder(servletHistory), "/history");
        handler.addServlet(new ServletHolder(servletLogout), "/logout");
        handler.addFilter(new FilterHolder(new NoUsersFilter(users)), "/login", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new ExistingLoginFilter(users)), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginFormatFilter()), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new PasswordFormatFilter()), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginPasswordFilter(users)), "/login", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new HasCookiesFilter()), "/calc", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new HasMyCookieFilter()), "/calc", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new CookieMatchFilter(users)), "/calc", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(CalculatorFilter.class, "/calc/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(CalcFilterByZero.class, "/calc/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));

        Server server = new Server(80);
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
