package users;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersActions {

    private Map<String, List<String>> usersActions = new HashMap<>();

    public List<String> getUserActions (String userId){
        return usersActions.get(userId);
    }
    public void addUser (String userId, List<String> list) {

        usersActions.put(userId, list);
    }
    public void addUserActions (String userId, String str){
        List<String> actions = usersActions.get(userId);
        actions.add(str);
        usersActions.put(userId, actions);
    }


}
