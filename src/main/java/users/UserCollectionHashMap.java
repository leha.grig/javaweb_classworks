package users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserCollectionHashMap implements UserCollection <User, Map<String, User>>{
    private Map<String, User> users = new HashMap<>();

    @Override
    public void addUser(User user) {
        users.put(user.getLogin(), user);
    }

    @Override
    public Map<String, User> getUsers() {
        return users;
    }

    @Override
    public List<User> getUsersList() {
        return new ArrayList<>(users.values());
    }

    @Override
    public User getUser(String userId) {
        return users.get(userId);
    }

    @Override
    public boolean check(String userLogin, String password) {
        return getUsersList().stream()
                .filter(u -> u.getLogin().equalsIgnoreCase(userLogin))
                .anyMatch(u -> u.getPassword().equals(password));
    }
}
