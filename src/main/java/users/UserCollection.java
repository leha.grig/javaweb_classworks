package users;

import java.util.List;

public interface UserCollection <T, M>{
    void addUser (T user);
    M getUsers ();
    List<T> getUsersList ();
    T getUser (String userId);
    boolean check (String userLogin, String password);
}
