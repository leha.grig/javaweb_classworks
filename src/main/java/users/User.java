package users;

import java.util.Objects;

public class User {
    private final String name;
    private final String surname;
    private final String login;
    private String password;
    private final int id;

    public User(String name, String surname, String login, String password) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.id = Math.abs(Objects.hash(login));
    }

    public boolean check(String password) {
        return password.equals(this.password);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
