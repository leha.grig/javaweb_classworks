package commands;

public interface Command {
    String getName();
    int doCommand(int x, int y);
    String getSymbol();
}
