package commands;


public class CommandAdd implements Command {
    private String name = MathActions.ADD.name();
    private String symbol = MathActions.ADD.getSymbol();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int doCommand(int x, int y) {
        return x + y;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }
}
