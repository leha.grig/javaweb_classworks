package commands;

public class CommandMultiply implements Command {
    private String name = MathActions.MUL.name();
    private String symbol = MathActions.MUL.getSymbol();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int doCommand(int x, int y) {
        return x * y;
    }
    @Override
    public String getSymbol() {
        return symbol;
    }
}
