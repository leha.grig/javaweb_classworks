package commands;

public class CommandSubtract implements Command {
    private String name = MathActions.SUB.name();
    private String symbol = MathActions.SUB.getSymbol();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int doCommand(int x, int y) {
        return x - y;
    }
    @Override
    public String getSymbol() {
        return symbol;
    }
}
