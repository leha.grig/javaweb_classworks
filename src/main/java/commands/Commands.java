package commands;

import java.util.ArrayList;
import java.util.List;

public class Commands {
    public static List<Command> allCommands (){
        return new ArrayList<Command>(){{
            add (new CommandAdd());
            add (new CommandDiv());
            add (new CommandMultiply());
            add (new CommandSubtract());
        }};
    }
}
