package commands;

public class CommandDiv implements Command {
    private String name = MathActions.DIV.name();
    private String symbol = MathActions.DIV.getSymbol();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int doCommand(int x, int y) {
        return x/y;
    }
    @Override
    public String getSymbol() {
        return symbol;
    }
}
