package commands;

import java.util.*;

public enum MathActions {
    ADD("+"), SUB("-"), MUL("*"), DIV("/");

    String symbol;

    MathActions (String symbol){
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    /*public Set<MathActions> getValues(){
        return EnumSet.allOf(MathActions.class);
    }*/
    public static List<String> getStringValues(){
        return new ArrayList<String>(){{
            for (int i = 0; i < values().length; i++) {
                add(values()[i].name());
            }
        }};
    }
}
